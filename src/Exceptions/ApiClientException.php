<?php namespace MoldersMedia\LightspeedPartnerApi\Exceptions;

use MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException as Exception;

class ApiClientException extends Exception
{
    /**
     * @var array
     */
    protected $error;

    /**
     * ApiClientException constructor.
     *
     * @param string $message
     * @param array $payload
     * @param string $resource
     * @param        $guzzleException
     * @param array $error
     */
    public function __construct($message, $payload, $resource, $guzzleException, array $error = null)
    {
        parent::__construct($message, $payload, $resource, $guzzleException);
        $this->error = $error;
    }

    /**
     * @return array
     */
    public function getError()
    {
        return $this->error;
    }
}