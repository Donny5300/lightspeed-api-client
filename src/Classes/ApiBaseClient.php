<?php namespace MoldersMedia\LightspeedPartnerApi\Classes;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Response;
use MoldersMedia\LightspeedApi\Classes\Api\ApiClient as Client;
use MoldersMedia\LightspeedPartnerApi\Exceptions\ApiClientException;

class ApiBaseClient extends Client
{
    /**
     * @var array
     */
    protected $apiSecretKeys = [];

    /**
     * @var int
     */
    protected $currentKeyIndex = 0;

    /**
     * @var int
     */
    protected $totalResets = 0;

    /**
     * @var array
     */
    protected $credentials = [];

    /**
     * @var array
     */
    protected $client;

    /** @noinspection PhpMissingParentConstructorInspection
     * @param array $keys
     */
    public function __construct(array $keys)
    {
        $this->registerResources();

        $this->prepareApiSecretKeys($keys)
            ->setApiRequirements($this->apiSecretKeys[$this->currentKeyIndex]);
    }

    /**
     * @param $apiCredentials
     * @return $this
     */
    private function setApiRequirements($apiCredentials)
    {
        $this->setApiSecret($apiCredentials['user_secret']);
        $this->setApiKey($apiCredentials['api_key']);
        $this->setApiServer('eu1');
        $this->setApiLanguage('nl');

        return $this;
    }

    /**
     * @param $client
     * @return $this
     */
    private function prepareApiSecretKeys($client)
    {
        foreach ($client as $key) {

            $secret                = md5($key['client_token'] . $key['api_secret']);
            $this->apiSecretKeys[] = [
                'requests_made' => 0,
                'api_key'       => $key['api_key'],
                'user_secret'   => $secret,
                'sleep_times'   => 0,
                'sleep_seconds' => [],
                'fails'         => 0
            ];
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getIncrementResets()
    {
        return $this->totalResets;
    }

    /**
     * @return array
     */
    public function getApiInformation()
    {
        $keys = $this->formatApiKeysForPublic($this->apiSecretKeys);

        return [
            'api_keys' => $keys,
            'statics'  => $this->formatStatics($keys),
        ];
    }

    /**
     * @param $apiSecretKeys
     * @return mixed
     */
    private function formatApiKeysForPublic($apiSecretKeys)
    {
        foreach ($apiSecretKeys as &$apiSecretKey) {
            $apiSecretKey['total_sleep_time'] = array_sum($apiSecretKey['sleep_seconds']) ?? 'error';
            $apiSecretKey['user_secret']      = '******************' . substr($apiSecretKey['user_secret'], 0, 6);
            $apiSecretKey['api_key']          = '******************' . substr($apiSecretKey['api_key'], 0, 6);
        }

        return $apiSecretKeys;
    }

    /**
     * @param $apiSecretKeys
     * @return array
     */
    private function formatStatics($apiSecretKeys)
    {
        $totalSleepTime = 0;
        $totalFails     = 0;

        foreach ($apiSecretKeys as $key) {
            $totalSleepTime += $key['total_sleep_time'];
            $totalFails     += $key['fails'];
        }

        return [
            'total_sleep_time' => $totalSleepTime
        ];
    }

    /**
     *
     */
    public function setNextKey()
    {
        $this->setNextKeyIndex();
        $this->setNextCredentials();
        $this->setApiRequirements($this->getCredentials());
    }

    /**
     * @return $this
     */
    private function setNextKeyIndex()
    {
        if (count($this->credentials) >= 2) {
            $this->currentKeyIndex++;
        }

        return $this;
    }

    /**
     * @return $this
     */
    private function setNextCredentials()
    {
        $credentials = $this->apiSecretKeys[$this->currentKeyIndex];

        $this->setCredentials($credentials);

        return $this;
    }

    /**
     * @return array
     */
    public function getCredentials()
    {
        return $this->credentials;
    }

    /**
     * @param array $credentials
     * @return $this
     */
    public function setCredentials(array $credentials)
    {
        $this->credentials = $credentials;

        return $this;
    }

    /**
     * @return array
     */
    public function getApiSecretKeys()
    {
        return $this->apiSecretKeys;
    }

    /**
     * @param $url
     * @param $method
     * @param $payload
     * @param $resource
     * @return Response
     * @throws ApiClientException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function makeRequest($url, $method, $payload, $resource): Response
    {
        $requestUrl = $this->makeRequestUrl($url);

        try {

            if ($this->getTestEnv()) {
                $payload['app_key'] = substr($this->getApiKey(), 0, 6);
            }

            $request = $this->makeGuzzleRequest($method, $requestUrl, $payload);

            $this->request = $request;
            $this->incrementRequestsMade();

            return $request;
        } catch (ClientException $exception) {

            $error = json_decode($exception->getResponse()->getBody()->getContents(), true);

            if ($error['error']['message'] == self::ERRORS_TOO_MANY_REQUESTS) {

                $this->registerFailForCurrentIndex();
                if ($this->shouldReset()) {
                    $secondForReset = $this->extractResetTime($exception->getResponse());
                    $this->registerSleepTime($secondForReset);
                    $this->reset();
                    $this->sleepHandler($secondForReset);
                }

                if ($this->hasMultipleKeys()) {
                    $this->setNextKeyIndex();
                    $this->setNextCredentials();
                    $this->setApiRequirements($this->getCredentials());
                }

                return $this->makeRequest($url, $method, $payload, $resource);
            }

            throw new ApiClientException($exception->getMessage(), $payload, $resource, $exception, $error);
        }
    }

    /**
     *
     */
    private function incrementRequestsMade()
    {
        $this->apiSecretKeys[$this->currentKeyIndex]['requests_made']++;
    }

    /**
     * @return $this
     */
    private function registerFailForCurrentIndex()
    {
        $this->apiSecretKeys[$this->currentKeyIndex]['fails']++;

        return $this;
    }

    /**
     * @return bool
     */
    private function shouldReset()
    {
        $totalAsIndex = (count($this->apiSecretKeys)) - 1; // 2 - 1 = 1
        $index        = $this->currentKeyIndex; // 1

        return $totalAsIndex === $index;
    }

    /**
     * @param $secondForReset
     * @return $this
     */
    private function registerSleepTime($secondForReset)
    {
        $this->apiSecretKeys[$this->currentKeyIndex]['sleep_times']++;
        $this->apiSecretKeys[$this->currentKeyIndex]['sleep_seconds'][] = $secondForReset;

        return $this;
    }

    /**
     *
     */
    private function reset()
    {
        $this->resetKeyIndex()
            ->setNextCredentials()
            ->setApiRequirements($this->getCredentials());

        return $this;
    }

    /**
     * @return $this
     */
    private function resetKeyIndex()
    {
        $this->currentKeyIndex = 0;

        return $this;
    }

    /**
     * @param $sleepTime
     * @return $this
     */
    private function sleepHandler($sleepTime)
    {
        sleep($sleepTime + 20);

        return $this;
    }

    /**
     * @return bool
     */
    public function hasMultipleKeys()
    {
        return count($this->apiSecretKeys) >= 2;
    }

    /**
     * @return $this
     */
    protected function incrementResets()
    {
        $this->totalResets++;

        return $this;
    }
}