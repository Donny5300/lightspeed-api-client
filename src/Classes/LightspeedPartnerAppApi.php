<?php namespace MoldersMedia\LightspeedPartnerApi;

use Carbon\Carbon;
use MoldersMedia\LightspeedPartnerApi\Classes\ApiBaseClient;

/**
 * Class LightspeedPartnerAppApi
 *
 * @package App\Classes\Lightspeed\Api
 */
class LightspeedPartnerAppApi extends ApiBaseClient
{
    const ITEMS_PER_PAGE = 250;

    /**
     * @param $productId
     * @param $key
     * @param $value
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function addProductVariable($productId, $key, $value)
    {
        return $this->productsMetafields->create($productId, [
            'key'   => $key,
            'value' => $value
        ]);
    }

    /**
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCategoryProductByPage($page)
    {
        return $this->categoriesProducts->get(null, [
            'page'  => $page,
            'limit' => 250
        ]);
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getTotalBrands()
    {

        return $this->brands->count();
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getTotalCategories()
    {

        return $this->categories->count();
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getTotalCategoryProduct()
    {
        return $this->categoriesProducts->count();
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getTotalProducts()
    {
        return $this->products->count();
    }

    /**
     * @param $metafieldId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteMetafield($metafieldId)
    {

        return $this->metafields->delete($metafieldId);
    }

    /**
     * @param array $array
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createWebhook(array $array): array
    {
        $array['format'] = 'json';

        return $this->webhooks->create($array);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteAllWebhooks()
    {
        foreach ($this->webhooks->get() as $webhook) {
            $this->webhooks->delete($webhook['id']);
        }
    }

    /**
     * @param $lightspeed_webhook_id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteWebhook($lightspeed_webhook_id)
    {

        return $this->webhooks->delete($lightspeed_webhook_id);
    }

    /**
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getOrdersByPage($page)
    {
        return $this->orders->get(null, ['page' => $page]);
    }

    /**
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getQuotesByPage($page)
    {
        return $this->quotes->get(null, ['page' => $page]);
    }

    /**
     * @param array $variants
     * @return $this
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function bulkUpdateVariantStock(array $variants)
    {
        $chunks = array_chunk($variants, 100);

        foreach ($chunks as $chunk) {

            $this->variantsBulk->update($chunk);
        }

        return $this;
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getWebhooks()
    {
        return $this->webhooks->get();
    }

    /**
     * @param $file
     * @return string
     */
    public function getAssetFile($file)
    {
        return 'https://' . config('app.url') . '/' . $file;
    }

    /**
     * @param        $script
     * @param string $location
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createScript($script, $location = 'body')
    {
        return $this->shopScripts
            ->create([
                'location' => $location,
                'url'      => $script
            ]);
    }

    /**
     * @return $this
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteScripts()
    {
        $scripts = $this->shopScripts->get();

        foreach ($scripts as $script) {
            $this->shopScripts->delete($script['id']);
        }

        return $this;
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getFilters()
    {
        return $this->filters->get(null, ['limit' => 250]);
    }

    /**
     * @param $productId
     * @param $attributes
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateProduct($productId, $attributes)
    {
        return $this->products->update($productId, $attributes);
    }

    /**
     * @param $productId
     * @param $metafieldId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteProductMetafield($productId, $metafieldId)
    {
        return $this->productsMetafields->delete($productId, $metafieldId);
    }

    /**
     * @param $id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getProductMetafields($id)
    {
        return $this->productsMetafields->get($id);
    }

    /**
     * @param $variantId
     * @param $stockLevel
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateVariantStock($variantId, $stockLevel)
    {
        return $this->variants->update($variantId, [
            'stockLevel' => $stockLevel
        ]);
    }

    /**
     * @param       $variantId
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateVariant($variantId, array $data)
    {
        return $this->variants->update($variantId, $data);
    }

    /**
     * @param $filterId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getFilterOptions($filterId)
    {
        return $this->filtersValues->get($filterId);
    }

    /**
     * @param $productId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getProductsWithFilters($productId)
    {
        return $this->productsFiltervalues->get($productId);
    }

    /**
     * @param array $product
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createProduct(array $product)
    {
        return $this->products->create($product);
    }

    /**
     * @param $hook
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteHook($hook)
    {
        return $this->webhooks->delete($hook);
    }

    /**
     * @param $productId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getFirstVariantByProductId($productId)
    {
        $variants = $this->getVariantsByProductId($productId);

        return array_first($variants);
    }

    /**
     * @param $productId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getVariantsByProductId($productId)
    {
        return $this->variants->get(null, ['product' => $productId]);
    }

    /**
     * @param $id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getProductById($id)
    {
        return $this->products->get($id);
    }

    /**
     * @param $productId
     * @param $getVariant
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function addVariant($productId, $getVariant)
    {
        $getVariant['product'] = $productId;

        if (empty($getVariant['title'])) {
            $getVariant['title'] = 'Default';
        }

        return $this->variants->create($getVariant);
    }

    /**
     * @param $id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getVariantById($id)
    {
        return $this->variants->get($id);
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getLanguagesFromCache()
    {
        $cacheSuffix = $this->client['id'];
        $cacheKey    = 'lightspeed_api_languages_' . $cacheSuffix;

        if (cache()->has($cacheKey)) {
            return cache()->get($cacheKey);
        }

        $languages = $this->getLanguages();

        cache()->put($cacheKey, $languages, 120);

        return $languages;
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getLanguages()
    {

        return $this->languages->get();
    }

    /**
     * @param $productId
     * @param $img
     * @param $title
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createImage($productId, $img, $title)
    {
        return $this->productsImages->create($productId, [
            'filename'   => $this->getImageTitle($title),
            'attachment' => $img
        ]);
    }

    /**
     * @param $title
     * @return string
     */
    protected function getImageTitle($title)
    {
        if (str_contains($title, '.jpg')) {
            return $title;
        }

        return $title . '.jpg';
    }

    /**
     * @param $productId
     * @return $this
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteProductImages($productId)
    {
        $images = $this->getProductImages($productId);

        foreach ($images as $image) {

            $this->productsImages->delete($productId, $image['id']);
        }

        return $this;
    }

    /**
     * @param $productId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getProductImages($productId)
    {
        return $this->productsImages->get($productId);
    }

    /**
     * @param $productId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function countProductImagesByProductId($productId)
    {
        return $this->productsImages->count($productId);
    }

    /**
     * @param $brand
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createBrand($brand)
    {
        return $this->brands->create($brand);
    }

    /**
     * @param $supplier
     * @return array|bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createSupplier($supplier)
    {
        return $this->suppliers->create($supplier);
    }

    /**
     * @param       $supplierId
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateSupplier($supplierId, array $data)
    {
        return $this->suppliers->update($supplierId, $data);
    }

    /**
     * @param $int
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCollectionByPage($int)
    {
        return $this->catalog->get(null, ['page' => $int]);
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCategoryStructure()
    {
        $categories = [];
        $totalPages = $this->getCategoryPages();

        for ($page = 1; $page <= $totalPages; $page++) {
            $categories = array_merge($this->getCategoriesByPage($page), $categories);
        }

        $categories = array_where($categories, function ($cat) {
            return !$cat['parent'];
        });

        $data = $this->renderRecurs($categories);

        return $data;
    }

    /**
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCategoryPages()
    {
        return $this->calculateTotalPages($this->categories->count());
    }

    /**
     * @param      $total
     * @param bool $perPage
     * @return int
     */
    public function calculateTotalPages($total, $perPage = false): int
    {
        if ($perPage) {
            return (int)ceil($total / $perPage);
        }

        return (int)ceil($total / self::ITEMS_PER_PAGE);
    }

    /**
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCategoriesByPage($page)
    {
        return $this->categories->get(null, [
            'page'  => $page,
            'limit' => 250
        ]);
    }

    /**
     * @param array $categories
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    private function renderRecurs(array &$categories)
    {
        foreach ($categories as $index => &$category) {
            $children = $this->categories->get(null, ['parent' => $category['id']]);
            if (count($children)) {
                $category['children'] = $this->renderRecurs($children);
                continue;
            }
            $category['children'] = false;

            continue;

        }

        return $categories;
    }

    /**
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCatalogPages()
    {
        return $this->calculateTotalPages($this->catalog->count());
    }

    /**
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCatalogByPage($page)
    {
        return $this->catalog->get(null, ['page' => $page, 'limit' => self::ITEMS_PER_PAGE]);
    }

    /**
     * @return \App\Classes\Lightspeed\Api\LightspeedPartnerAppApi
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteAllCategories(): self
    {
        $pages = $this->getCategoryPages();

        for ($page = 1; $page <= $pages; $page++) {
            foreach ($this->getCategoriesByPage($page) as $category) {

                $this->deleteCategoryById($category['id']);
            }
        }

        return $this;
    }

    /**
     * @param $categoryId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteCategoryById($categoryId)
    {

        return $this->categories->delete($categoryId);
    }

    /**
     * @param $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createCategory($data)
    {
        return $this->categories->create($data);
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getAllCategories()
    {
        $categories = [];

        $pages = $this->getCategoryPages();

        for ($page = 1; $page <= $pages; $page++) {

            $data = $this->getCategoriesByPage($page);

            $categories = array_merge($data, $categories);
        }

        return $categories;
    }

    /**
     * @param $categoryId
     * @param $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateCategory($categoryId, $data)
    {
        return $this->categories->update($categoryId, $data);
    }

    /**
     * @param $id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCategoryImageByCategoryId($id)
    {

        return $this->categoriesImage->get($id);
    }

    /**
     * @param $id
     * @param $encoded
     * @param $title
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createCategoryImage($id, $encoded, $title)
    {

        return $this->categoriesImage->create($id, [
            'attachment' => $encoded,
            'filename'   => $title
        ]);
    }

    /**
     * @return $this
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteAllProducts()
    {
        $pages = $this->getProductPages();

        for ($page = 1; $page <= $pages; $page++) {
            foreach ($this->getProductsByPage($page) as $product) {

                $this->deleteProduct($product['id']);
            }
        }

        return $this;
    }

    /**
     * @param array $params
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getProductPages(array $params = [], $perPage = false)
    {
        $products = $this->products->count($params);

        return $this->calculateTotalPages($products, $perPage);
    }

    /**
     * @param       $page
     * @param array $params
     * @param bool $perPage
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getProductsByPage($page, array $params = [], $perPage = false)
    {
        return $this->products->get(null, [
                                              'page'  => $page,
                                              'limit' => $perPage ? $perPage : self::ITEMS_PER_PAGE
                                          ] + $params);
    }

    /**
     * @param $id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteProduct($id)
    {
        return $this->products->delete($id);
    }

    /**
     * @return $this
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteAllBrands()
    {
        $pages = $this->getBrandPages();

        for ($page = 1; $page <= $pages; $page++) {
            foreach ($this->getBrandsByPage($page) as $product) {

                $this->deleteBrand($product['id']);
            }
        }

        return $this;
    }

    /**
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getBrandPages()
    {
        $brands = $this->brands->count();

        return $this->calculateTotalPages($brands);
    }

    /**
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getBrandsByPage($page)
    {
        return $this->brands->get(null, [
            'page'  => $page,
            'limit' => self::ITEMS_PER_PAGE
        ]);
    }

    /**
     * @param $brandId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    private function deleteBrand($brandId)
    {

        return $this->brands->delete($brandId);
    }

    /**
     * @param       $brandId
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateBrand($brandId, array $data)
    {
        return $this->brands->update($brandId, $data);
    }

    /**
     * @return \App\Classes\Lightspeed\Api\LightspeedPartnerAppApi
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteAllSuppliers()
    {
        $pages = $this->getSupplierPages();

        for ($page = 1; $page <= $pages; $page++) {
            foreach ($this->getSuppliersByPage($page) as $supplier) {

                $this->deleteSupplierById($supplier['id']);
            }
        }

        return $this;
    }

    /**
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getSupplierPages()
    {
        $suppliers = $this->suppliers->count();

        return $this->calculateTotalPages($suppliers);
    }

    /**
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getSuppliersByPage($page)
    {

        return $this->suppliers->get(null, ['page' => $page, 'limit' => self::ITEMS_PER_PAGE]);
    }

    /**
     * @param $id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteSupplierById($id)
    {

        return $this->suppliers->delete($id);
    }

    /**
     * @param $propertyPlural
     * @return $this
     * @throws \Exception
     */
    public function deleteAll($propertyPlural)
    {

        $pages = $this->calculatePages($propertyPlural);

        for ($page = 1; $page <= $pages; $page++) {

            foreach ($this->{'get' . $propertyPlural . 'ByPage'}($page) as $item) {

                $this->deleteItem($propertyPlural, $item['id']);
            }
        }

        return $this;
    }

    /**
     * @param $property
     * @param $id
     * @return mixed
     */
    public function deleteItem($property, $id)
    {

        return $this->{camel_case($property)}->delete($id);
    }

    /**
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createDeliverydate(array $data)
    {

        return $this->deliverydates->create($data);
    }

    /**
     * @param $id
     * @param $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateDeliverydate($id, $data)
    {
        return $this->deliverydates->update($id, $data);
    }

    /**
     * @param array $data
     * @param null $productId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createVariant(array $data, $productId = null)
    {
        $data['title'] = rtrim(trim($data['title']));

        if (empty($data['title'])) {
            $data['title'] = 'Default';
        }

        if ($productId) {
            $data['product'] = $productId;
        }

        return $this->variants->create($data);
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function countCategoryProduct()
    {

        return $this->categoriesProducts->count();
    }

    /**
     * @param $categoryId
     * @param $productId
     * @param $sortOrder
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createCategoryProduct($categoryId, $productId, $sortOrder)
    {

        return $this->categoriesProducts->create([
            'product'   => $productId,
            'category'  => $categoryId,
            'sortOrder' => $sortOrder
        ]);
    }

    /**
     * @param $quoteId
     * @param $productId
     * @param $variantId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function addToQuote($quoteId, $productId, $variantId)
    {

        return $this->quotesProducts->create($quoteId, [
            'product' => $productId,
            'variant' => $variantId
        ]);
    }

    /**
     * @param $string
     * @return string
     */
    public function getUniqueCacheKey($string)
    {
        return $string . '_' . $this->client['id'];
    }

    /**
     * @param $sourceId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCategoryById($sourceId)
    {

        return $this->categories->get($sourceId);
    }

    /**
     * @param $fieds
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createQuote($fieds)
    {

        return $this->quotes->create($fieds);
    }

    /**
     * @param $quoteId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getQuote($quoteId)
    {

        return $this->quotes->get($quoteId);
    }

    /**
     * @param $quoteId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getQuoteProducts($quoteId)
    {

        return $this->quotesProducts->get($quoteId);
    }

    /**
     * @param $quoteId
     * @param $productId
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteProductFromQuote($quoteId, $productId)
    {

        $this->quotesProducts->delete($quoteId, $productId);
    }

    /**
     * @param $orderId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getOrder($orderId)
    {

        return $this->orders->get($orderId);
    }

    /**
     * @param $id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCustomer($id)
    {

        return $this->customers->get($id);
    }

    /**
     * @param $date
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function calculatePagesWithUpdatedProducts($date)
    {

        $products = $this->products->count([
            'updated_at_min' => $date,
            'limit'          => self::ITEMS_PER_PAGE
        ]);

        return $this->calculateTotalPages($products);
    }

    /**
     * @param $date
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getProductsByUpdatedAt($date, $page)
    {

        return $this->products->get(null, [
            'updated_at_min' => $date,
            'limit'          => self::ITEMS_PER_PAGE,
            'page'           => $page
        ]);
    }

    /**
     * @param $date
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function calculatePagesWithUpdatedVariants($date)
    {

        $products = $this->variants->count([
            'updated_at_min' => $date,
            'limit'          => self::ITEMS_PER_PAGE
        ]);

        return $this->calculateTotalPages($products);
    }

    /**
     * @param $date
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getVariantsByUpdatedAt($date, $page)
    {

        return $this->variants->get(null, [
            'updated_at_min' => $date,
            'limit'          => self::ITEMS_PER_PAGE,
            'page'           => $page
        ]);
    }

    /**
     * @param $date
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCategoriesByUpdatedAt($date, $page)
    {

        return $this->categories->get(null, [
            'updated_at_min' => $date,
            'limit'          => self::ITEMS_PER_PAGE,
            'page'           => $page
        ]);
    }

    /**
     * @param $date
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function calculatePagesWithUpdatedCategories($date)
    {

        $products = $this->categories->count([
            'updated_at_min' => $date,
            'limit'          => self::ITEMS_PER_PAGE
        ]);

        return $this->calculateTotalPages($products);
    }

    /**
     * @param $date
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getBrandsByUpdatedAt($date, $page)
    {

        return $this->brands->get(null, [
            'updated_at_min' => $date,
            'limit'          => self::ITEMS_PER_PAGE,
            'page'           => $page
        ]);
    }

    /**
     * @param $date
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function calculatePagesWithUpdatedBrands($date)
    {

        $brands = $this->brands->count([
            'updated_at_min' => $date,
            'limit'          => self::ITEMS_PER_PAGE
        ]);

        return $this->calculateTotalPages($brands);
    }

    /**
     * @param $variantId
     * @param $title
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateVariantTitle($variantId, $title)
    {

        return $this->variants->update($variantId, ['title' => $title]);
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->getApiLanguage();
    }

    /**
     * @param $languageAbbreviation
     * @return $this
     */
    public function setLanguage($languageAbbreviation)
    {
        $this->setApiLanguage($languageAbbreviation);
        $this->language = $languageAbbreviation;

        return $this;
    }

    /**
     * @param $id
     * @param $webhook
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateWebhook($id, $webhook)
    {

        return $this->webhooks->update($id, $webhook);
    }

    /**
     * @param $date
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function calculatePagesWithUpdatedCustomers($date)
    {

        $products = $this->customers->count([
            'updated_at_min' => $date,
            'limit'          => self::ITEMS_PER_PAGE
        ]);

        return $this->calculateTotalPages($products);
    }

    /**
     * @param $date
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCustomersByUpdatedAt($date, $page)
    {

        return $this->customers->get(null, [
            'updated_at_min' => $date,
            'limit'          => self::ITEMS_PER_PAGE,
            'page'           => $page
        ]);
    }

    /**
     * @param $date
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function calculatePagesWithUpdatedGroups($date)
    {

        $products = $this->groups->count([
            'updated_at_min' => $date,
            'limit'          => self::ITEMS_PER_PAGE
        ]);

        return $this->calculateTotalPages($products);
    }

    /**
     * @param $date
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getGroupsByUpdatedAt($date, $page)
    {

        return $this->groups->get(null, [
            'updated_at_min' => $date,
            'limit'          => self::ITEMS_PER_PAGE,
            'page'           => $page
        ]);
    }

    /**
     * @param $id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCustomerGroupByCustomerId($id)
    {

        return $this->groupsCustomers->get(null, ['customer' => $id]);
    }

    /**
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCustomerGroupByPage($page)
    {

        return $this->groupsCustomers->get(null, ['page' => $page, 'limit' => self::ITEMS_PER_PAGE]);
    }

    /**
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getGroupsByPage($page)
    {

        return $this->groups->get(null, ['page' => $page, 'limit' => self::ITEMS_PER_PAGE]);
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getAllBrands()
    {
        $pages = $this->getBrandPages();

        $brands = [];

        for ($page = 1; $page <= $pages; $page++) {

            $apiBrands = $this->getBrandsByPage($page);
            $brands    = array_merge($brands, $apiBrands);
        }

        return $brands;
    }

    /**
     * @param $id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteBrandById($id)
    {

        return $this->brands->delete($id);
    }

    /**
     * @param $brandId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getBrandById($brandId)
    {

        return $this->brands->get($brandId);
    }

    /**
     * @param $item
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createFilter($item)
    {
        if (empty($item['title'])) {
            $item['title'] = 'Unknown';
        }

        return $this->filters->create($item);
    }

    /**
     * @param $id
     * @param $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateFilter($id, $data)
    {
        return $this->filters->update($id, $data);
    }

    /**
     * @param $itemId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getFilterById($itemId)
    {

        return $this->filters->get($itemId);
    }

    /**
     * @param $filterId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getFilterValuesByFilterId($filterId)
    {

        return $this->filtersValues->get($filterId);
    }

    /**
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getTypeAttributesByPage($page)
    {

        return $this->typesAttributes->get(null, ['page' => $page]);
    }

    /**
     * @param Carbon $yesteray
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function calculateProductsUpdatedYesterday(Carbon $yesteray)
    {

        $count = $this
            ->products
            ->count([
                'updated_at_min' => $yesteray->startOfDay(),
                'updated_at_max' => $yesteray->copy()->endOfDay()
            ]);

        return $this->calculateTotalPages($count);
    }

    /**
     * @param $id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getProductAttributesByProductId($id)
    {

        return $this->productsAttributes->get($id);
    }

    /**
     * @param $itemId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getAttributeById($itemId)
    {

        return $this->attributes->get($itemId);
    }

    /**
     * @param $id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteAttributeById($id)
    {

        return $this->attributes->delete($id);
    }

    /**
     * @param $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createAttribute($data)
    {

        return $this->attributes->create($data);
    }

    /**
     * @param $id
     * @param $newItem
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateAttribute($id, $newItem)
    {

        return $this->attributes->update($id, $newItem);
    }

    /**
     * @param $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createType($data)
    {

        return $this->types->create($data);
    }

    /**
     * @param $id
     * @param $newItem
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateType($id, $newItem)
    {

        return $this->types->update($id, $newItem);
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getAllAttributes()
    {
        $attributes = [];

        $pages = $this->getAttributePages();

        for ($page = 1; $page <= $pages; $page++) {

            $data = $this->getAttributesByPage($page);

            $attributes = array_merge($data, $attributes);
        }

        return $attributes;
    }

    /**
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getAttributePages()
    {
        return $this->calculateTotalPages($this->attributes->count());

    }

    /**
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getAttributesByPage($page)
    {

        return $this->attributes->get(null, ['page' => $page]);
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getAllTypes()
    {
        $types = [];

        $pages = $this->getTypePages();

        for ($page = 1; $page <= $pages; $page++) {

            $types = array_merge($this->getTypesByPage($page), $types);
        }

        return $types;
    }

    /**
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getTypePages()
    {
        return $this->calculateTotalPages($this->types->count());
    }

    /**
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getTypesByPage($page)
    {

        return $this->types->get(null, ['page' => $page]);
    }

    /**
     * @param $itemId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getTypeById($itemId)
    {

        return $this->types->get($itemId);
    }

    /**
     * @param $itemId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getDeliveryDateById($itemId)
    {

        return $this->deliverydates->get($itemId);
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getAllDeliverydates()
    {
        $types = [];

        $pages = $this->getDeliverydatePages();

        for ($page = 1; $page <= $pages; $page++) {

            $types = array_merge($this->getDeliverydatesByPage($page), $types);
        }

        return $types;
    }

    /**
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getDeliverydatePages()
    {
        $deliverydates = $this->deliverydates->count();

        return $this->calculateTotalPages($deliverydates);
    }

    /**
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getDeliverydatesByPage($page)
    {

        return $this->deliverydates->get(null, ['limit' => self::ITEMS_PER_PAGE, 'page' => $page]);
    }

    /**
     * @param $itemId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getSupplierById($itemId)
    {

        return $this->suppliers->get($itemId);
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getAllSuppliers()
    {
        $suppliers = [];

        return $this->suppliers->get(null, ['limit' => self::ITEMS_PER_PAGE]);
        $pages = $this->getSupplierPages();

        for ($page = 1; $page <= $pages; $page++) {
            $suppliers = array_merge($this->getSuppliersByPage($page), $suppliers);
        }

        return $suppliers;
    }

    /**
     * @param $productId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getRelatedProducts($productId)
    {

        return $this->productsRelations->get($productId);
    }

    /**
     * @param $productId
     * @param $relatedProductId
     * @param $sortOrder
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createRelatedProduct($productId, $relatedProductId, $sortOrder = null)
    {
        $data = [
            'relatedProduct' => $relatedProductId
        ];

        if ($sortOrder) {
            $data['sortOrder'] = $sortOrder;
        }

        return $this->productsRelations->create($productId, $data);
    }

    /**
     * @param $productId
     * @param $relationId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteRelatedProduct($productId, $relationId)
    {

        return $this->productsRelations->delete($productId, $relationId);
    }

    /**
     * @param $sourceProductId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCategoryProductByProductId($sourceProductId)
    {

        return $this->categoriesProducts->get(null, ['product' => $sourceProductId]);
    }

    /**
     * @param $sourceProductId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCategoryProducts($sourceProductId)
    {

        return $this->categoriesProducts->get(null, ['product' => $sourceProductId]);
    }

    /**
     * @param $pivotId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteCategoryProduct($pivotId)
    {

        return $this->categoriesProducts->delete($pivotId);
    }

    /**
     * @param $productId
     * @param $pivotId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteProductFilterValue($productId, $pivotId)
    {

        return $this->productsFiltervalues->delete($productId, $pivotId);
    }

    /**
     * @param $productId
     * @param $filterValueId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createProductFilterValue($productId, $filterValueId)
    {
        return $this->productsFiltervalues->create($productId, [
            'filtervalue' => $filterValueId
        ]);
    }

    /**
     * @param $productId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getProductFilterValues($productId)
    {

        return $this->productsFiltervalues->get($productId);
    }

    /**
     * @param array $data
     * @param       $filterId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createFilterValue(array $data, $filterId)
    {
        if (empty($data['title'])) {
            $data['title'] = 'Unknown';
        }

        return $this->filtersValues->create($filterId, $data);
    }

    /**
     * @param $filterId
     * @param $filterValueId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getFilterValueById($filterId, $filterValueId)
    {

        return $this->filtersValues->get($filterId, $filterValueId);
    }

    /**
     * @param $filterId
     * @param $filterValueId
     * @param $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateFilterValue($filterId, $filterValueId, $data)
    {

        return $this->filtersValues->update($filterId, $filterValueId, $data);
    }

    /**
     * @param $filterId
     * @param $filterValueId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteFilterValueById($filterId, $filterValueId)
    {

        return $this->filtersValues->delete($filterId, $filterValueId);
    }

    /**
     * @param $productId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getFilterProduct($productId)
    {

        return $this->productsFiltervalues->get($productId);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteAllDeliverytimes()
    {
        $pages = $this->getDeliverydatePages();

        for ($page = 1; $page <= $pages; $page++) {
            foreach ($this->getDeliverydatesByPage($page) as $deliverydate) {

                $this->deleteDeliverydateById($deliverydate['id']);
            }
        }
    }

    /**
     * @param $id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteDeliverydateById($id)
    {

        return $this->deliverydates->delete($id);
    }

    /**
     */
    public function deleteAllVariants()
    {

        $pages = $this->calculatePages('variants');

        for ($page = 1; $page <= $pages; $page++) {
            foreach ($this->getVariantsByPage($page) as $variant) {

                $this->deleteVariant($variant['id']);
            }
        }

    }

    /**
     * @param $page
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getVariantsByPage($page)
    {

        return $this->variants->get(null, [
            'page'  => $page,
            'limit' => 250
        ]);
    }

    /**
     * @param $variantId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteVariant($variantId)
    {

        return $this->variants->delete($variantId);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteAllFilters()
    {
        foreach ($this->getAllFilters() as $variant) {
            $this->deleteFilterById($variant['id']);
        }
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getAllFilters()
    {

        return $this->filters->get(null, ['limit' => 250]);
    }

    /**
     * @param $id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function deleteFilterById($id)
    {

        return $this->filters->delete($id);
    }

    /**
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getVariantPages()
    {
        $variants = $this->variants->count();

        return $this->calculateTotalPages($variants);
    }

    /**
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getTypeAttributePages()
    {
        return $this->calculateTotalPages($this->typesAttributes->count());
    }

    /**
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCustomerGroupPages()
    {
        return $this->calculateTotalPages($this->groups->count());
    }

    /**
     * @param $brandId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getBrandImage($brandId)
    {
        return $this->brandsImage->get($brandId);
    }

    public function updateBrandImage($destinationItemId, $destinationTitle)
    {

    }

    /**
     * @param $brandId
     * @param $contents
     * @param $title
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createBrandImage($brandId, $contents, $title)
    {
        $this->brandsImage->create($brandId, [
            'attachment' => $contents,
            'filename'   => $title . '.png'
        ]);
    }

    /**
     * @param $id
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getProductForOrder($id)
    {
        return $this->ordersProducts->get($id);
    }

    /**
     * @param $categoryId
     * @param $contents
     * @param $title
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateCategoryImage($categoryId, $contents, $title)
    {
        $this->categoriesImage->create($categoryId, [
            'attachment' => $contents,
            'filename'   => $title . '.png'
        ]);
    }

    /**
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCategoryProductPages()
    {
        return $this->calculateTotalPages($this->categoriesProducts->count());
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getTotalVariants()
    {
        return $this->variants->count();
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getTotalSuppliers()
    {
        return $this->suppliers->count();
    }

    /**
     * @param       $productId
     * @param array $values
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createMetafield($productId, array $values)
    {
        return $this->productsMetafields->create($productId, $values);
    }

    /**
     * @param $destinationProductId
     * @param $id
     * @param $value
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateMetafieldValue($destinationProductId, $id, $value)
    {
        return $this->productsMetafields->update($destinationProductId, $id, [
            'value' => $value
        ]);
    }

    /**
     * @param $productId
     * @param $imageId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getProductImageById($productId, $imageId)
    {
        return $this->productsImages->get($productId, $imageId);
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getCategoryMetafields()
    {
        $data  = [];
        $total = $this->metafields->count();
        $pages = $this->calculateTotalPages($total);

        for ($page = 1; $page <= $pages; $page++) {
            $categories = $this->metafields->get(null, ['page' => $page]);

            $categories = array_where($categories, function ($meta) {
                return $meta['ownerType'] == 'category';
            });

            $data = array_merge($data, $categories);
        }

        return $data;
    }

    /**
     * @param $categoryId
     * @param $key
     * @param $value
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createCategoryMetafield($categoryId, $key, $value)
    {
        return $this->metafields->create([
            'ownerType' => 'category',
            'ownerId'   => $categoryId,
            'key'       => $key,
            'value'     => $value
        ]);
    }

    /**
     * @param $metafieldId
     * @param $categoryId
     * @param $key
     * @param $value
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateCategoryMetafield($metafieldId, $categoryId, $key, $value)
    {
        return $this->metafields->update($metafieldId, [
            'ownerType' => 'category',
            'ownerId'   => $categoryId,
            'value'     => $value,
            'key'       => $key
        ]);
    }

    /**
     * @param $productId
     * @param $value
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function updateData03($productId, $value)
    {
        return $this->products->update($productId, [
            'data_03' => $value,
            'data03'  => $value,
        ]);
    }

    /**
     * @param $variantId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function setDefaultVariant($variantId)
    {
        return $this->variants->update($variantId, [
            'isDefault' => true
        ]);
    }

    /**
     * @param $productId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function hideProduct($productId)
    {
        return $this->products->update($productId, [
            'isVisible'  => false,
            'visibility' => 'hidden'
        ]);
    }

    /**
     * @param $from
     * @param $to
     * @param $permanent
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createRedirect($from, $to, $permanent)
    {
        return $this->redirects->create([
            'isPermanent' => $permanent,
            'url'         => $from,
            'target'      => $to,
        ]);
    }

    /**
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getOrderPages()
    {
        $orders = $this->orders->count();

        return $this->calculateTotalPages($orders);
    }

    /**
     * @param $orderId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getProductsByOrderId($orderId)
    {
        return $this->ordersProducts->get($orderId);
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function getTaxes()
    {
        return $this->taxes->get(null, [
            'limit' => self::ITEMS_PER_PAGE
        ]);
    }

    /**
     * @param $orderId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function markOrderAsShipped($orderId)
    {
        return $this->orders->update($orderId, [
            'shipmentStatus' => 'shipped'
        ]);
    }

    /**
     * @param $categoryId
     * @param $productId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiClientException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiLimitReachedException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\ApiSleepTimeException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\General\InvalidApiCredentialsException
     * @throws \MoldersMedia\LightspeedApi\Classes\Exceptions\Resources\SupplierExistsException
     */
    public function createCategoryProductWithoutSortOrder($categoryId, $productId)
    {
        return $this->categoriesProducts->create([
            'product'  => $productId,
            'category' => $categoryId
        ]);
    }

    public function setOrderMemo($orderId, $memo)
    {
        return $this->orders->update($orderId, [
            'memo' => $memo
        ]);
    }
}
