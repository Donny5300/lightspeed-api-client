# Quick start

The ApiBaseClient is the first layer for the API connection. This class contains methods to add a sleep or delay when the request limit has been reached. It also allows you to add multiple API keys for more API requests per minute.